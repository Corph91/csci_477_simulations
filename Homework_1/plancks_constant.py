from __future__ import division, print_function
from numpy import loadtxt, mean, unique, poly1d, polyfit
import matplotlib.pyplot as plt
x,y = loadtxt("millikan.txt"),loadtxt("millikan.txt",usecols=[1])

electron_charge = 1.602 * (10**-19)
e_x = mean(x)
e_y = mean(y)
e_xx = mean(x**2)
e_xy = mean(x * y)

m = (e_xy - (e_x * e_y))/(e_xx - (e_x**2))
c = ((e_xx * e_y) - (e_x * e_xy))/(e_xx - (e_x**2))
y = (m * x) - c

p_const = ((y + c)/x) * electron_charge
print("Planick's constant is approximately: ",p_const[0])
plt.plot(unique(x), poly1d(polyfit(x, y, 1))(unique(x)))
plt.scatter(x,y)
plt.xlabel(r'$\nu$')
plt.ylabel(r'$V$')
plt.title("Photoelectric Effect")
plt.show()
