
def load_words(words):
    with open('words.txt' ,'r+') as word_file:
        for line in word_file:
            line = line.split()
            for word in line:
                words.append(word)
        word_file.close()
    return words

def filter_words(words, normal_words, pallindromes):
    for word in words:
        if (word[::-1] in words) and (word[::-1] != word):
            normal_words.append(word)
        elif (word[::-1] in words) and (word[::-1] == word):
            pallindromes.append(word)

    return normal_words, pallindromes

def build_three(normal_words,pallindromes, words):
    test_word = ''
    final_words=[]
    i = 0
    for word_one in normal_words:
        stop = False
        for word_two in normal_words:
            reversed_one = word_one[::-1]
            if (word_two != word_one) and (word_two != word_one[::-1]):
                reversed_two = word_two[::-1]
                if word_one[1] == word_two[0] and word_one[3] == word_two[4] and reversed_two[3]:
                    for word_three in pallindromes:
                        if word_three[1] == word_two[2] and word_three[0] == word_one[2]:
                            test_word = word_one +word_two + word_three + word_two[::-1] + word_one[::-1]
                            if(test_word == test_word[::-1]):
                                stop = True
                                final_words.append(test_word)
                                i +=1
                                break
                if stop:
                    break
    return final_words



def print_squares(sator_squares):
    print("\nThere are: "+str(len(sator_squares))+" SATOR squares.\n")
    for square in sator_squares:

        print("SATOR Square for:\n\n"+square[0:5]+"\n"+square[5:10]+"\n"+square[10:15]+"\n")
        print(square[0],square[1],square[2],square[3],square[4])
        print(square[5],square[6],square[7],square[8],square[9])
        print(square[10], square[11], square[12], square[13], square[14])
        print(square[15], square[16], square[17], square[18], square[19])
        print(square[20], square[21], square[22], square[23], square[24])

        print("\n\n")

def main():
    words = []
    normal_words = []
    pallindromes = []


    words = load_words(words)
    normal_words,pallindromes = filter_words(words,normal_words,pallindromes)
    sator_squares = build_three(normal_words,pallindromes,words)
    print_squares(sator_squares)

main()