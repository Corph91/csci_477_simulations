"""
    Sean Corbett
    04/26/2017
    CSCI477: Simulations
    Homework4: Implicit Method
"""

from __future__ import division
import numpy as np, visual as vp, vpm
import scipy.sparse as sparse
from scipy.sparse.linalg import spsolve

# As coded before for setup of matrices and initial values and boundary conditions


def gaussian(x,y,mu_x = 0,mu_y=0,sigma=.3):
    return np.exp(-(np.sqrt((x-mu_x)**2+(y-mu_y)**2))**2/(2*sigma**2))

N = 25
L = 1
B = 0.5
x = np.linspace(-L/2., L/2., N)
y = np.linspace(-L/2., L/2., N)
x, y = np.meshgrid(x, y)

def init():
    global u0,u1,A,Aprime,boundary
    init_value = gaussian(x,y)
    u0, u1 = init_value.flatten(),init_value.flatten()

    A = np.diag(-4 * np.ones(N ** 2))
    A += np.diag(np.ones((N ** 2) - 1), 1)
    A += np.diag(np.ones((N ** 2) - 1), -1)
    A += np.diag(np.ones(N * (N-1)),N)
    A += np.diag(np.ones(N * (N-1)),-N)

    I = np.diag(np.ones(N**2))

    b = range(N**2-N,N**2)
    t = range(0,N)
    l = range(0, N**2-N,N)
    r = range(N-1,N**2,N)
    boundary = b + t + l + r
    A[boundary,:] = I[boundary,:]
    Aprime = (I - B**2 * A)
    Aprime[boundary, :] = I[boundary, :]
    A = sparse.csr_matrix(A)
    Aprime = sparse.csr_matrix(Aprime)

def update(u0,u1):
    b = (B**2) * A.dot(u1) + 2 * u1 - u0
    return spsolve(Aprime,b)

init()

scene = vp.display(title='2D waves', background=(.2,.5,1),
                   center=(L/2,L/2,0), up=(0,0,1), forward=(1,2,-1))

scene.material = vp.materials.marble # default material for all objects
net = vpm.net(x, y, 2*u0.reshape(N,N), vp.color.yellow, 0.005)              # mesh net
#mesh = vpm.mesh(x, y, u0.reshape(N,N), (1.0,1.0,1.0), (1,0,0))   # mesh

while(True):
    vp.rate(40), vpm.wait(scene)
    un = update(u0, u1)
    un[boundary] = 0
    net.move(x, y, un.reshape(N, N))
    u0, u1 = u1,un