import pylab as pl
from scipy.sparse import csr_matrix,lil_matrix
import numpy as np


def rk4(f,x,t,dt):
    k1 = f(x,t)
    k2 = f(x+dt/2.*k1,t+dt/2.)
    k3 = f(x+dt/2.*k2,t+dt/2.)
    k4 = f(x+dt*k3,t+dt)
    return x + dt/6.*(k1+2*k2+2*k3+k4)

# The discretization of the problem (use these to avoid trouble with instability)
N  =  25   # Space domain will be N x N
dt = .10    # Time step in seconds
L  = .11/2.    # Radius of simulated area, large enough to place a 5 cm radius round kettle inside.

x,y   =  abs(np.linspace(-L,L,N)), abs(np.linspace(-L,L,N))
print(x,y)
xg,yg =  np.meshgrid(x,y)
xg, yg = xg.flatten(), yg.flatten()

dx = L/float(N)
# The differential operator
A = (np.diag(-4 * np.ones(N**2)) + np.diag(np.ones(N**2 - 1),1) + np.diag(np.ones(N**2 - 1),-1) + np.diag(np.ones(N * (N-1)),N) + np.diag(np.ones(N * (N-1)),-N))
A = csr_matrix(A)
# Physical features:
k          = 1.172e-5     # thermal diffusivity in m**2/s for 1% carbon steel
cp         = .446e3       # Heat capacity of steel in J/(K kg)
rho        = 8050         # Density of steel in kg/m**2
room_temp  = 23.5         # room temperature
final_temp = 125          # desired temperature at the center of the plate
thickness  = .002         # thickness of steel plate m
power      = 1200         # Power of kettle, in Watts

def outside(xg,yg,rout):
    # This functions accepts the x and y grids
    # it returns indices of grid points that lie OUTSIDE
    # the radius specified, rout.
    # The idea is that this function is called and returns the indices
    # that are later set to be room temperature. HINT: use pl.find
    return np.unique(np.concatenate((pl.find(xg[:N**2] > rout),pl.find(yg[:N**2] > rout))),0)

def source(x,y,heat,rmin=.02,rmax=.04):
    # Function to produce the heat source -
    # does a pair of logical tests on r, the radius of the kettle,
    # starting from 0 at the center.
    # Returns a matrix of zeros except where r is between
    # rmin and rmax. There it returns heat.
    # HINT: Turn a logical array to a float one with array.astype(float)
    return ((np.sqrt(x**2 + y**2)[:N**2] >= rmin) & (np.sqrt(x**2 + y**2)[:N**2] <= rmax)).astype(float) * heat


# initial temperature (room temperature), known on a N**2 x 1 vector
u0 = room_temp * np.ones(N**2)

# Heat source, careful with dimensions here:

# Heat from hot plate: should be watts generated divided by
# area of element times thickness of steel plate
# divided by rho times cp
area_elem = np.pi * ((0.04**2) - (0.02**2))
heat = (power/(area_elem * thickness))/(rho * cp)
print(heat)
src = source(xg,yg,heat)
# Determine which nodes are outside the kettle
OUTSIDE = outside(xg,yg,.04)
# This is the function that is passed to rk4, it finds the new rates of change of
# temperature, as per the partial differential equation.
# Setting the values outside the kettle to zero in this function will prevent
# them from changing from thier initial values.

def heat_eqn(u,t):
    u_p = k/(dx**2) * A.dot(u) + src
    u_p[OUTSIDE] = 0
    return u_p

mid = (int)((N**2)/2.)
un = []
un.append(u0)
t = 0
count = 1

while(un[count-1][mid] <= final_temp):
    un.append(rk4(heat_eqn,un[count-1],t,dt))
    t += dt
    count += 1



def plot_four():
    """
        It's a bit of a waste to loop through every value in the un matrix,
        so I've carefully selected graphs at times of t1, tn/3, 2*tn/3, and tn.
    :return:
    """
    x, y = np.linspace(-L, L, N), np.linspace(-L, L, N)
    xg, yg = np.meshgrid(x, y)
    pl.contourf(xg,yg,un[1].reshape(N,N),cmap='inferno')
    cb = pl.colorbar()
    cb.set_label(r"Degrees C$^{\circ}$")
    pl.title("Time: %s seconds"%(1*dt))
    pl.show()

    pl.contourf(xg,yg,un[(int)((len(un)-1)/3)].reshape(N,N),cmap='inferno')
    cb = pl.colorbar()
    cb.set_label(r"Degrees C$^{\circ}$")
    pl.title("Time: %s seconds"%((int)((len(un)-1)/3)))
    pl.show()

    pl.contourf(xg,yg,un[(int)(2 * (len(un)-1)/3)].reshape(N,N),cmap='inferno')
    cb = pl.colorbar()
    cb.set_label(r"Degrees C$^{\circ}$")
    pl.title("Time: %s seconds"%((int)(2 * (len(un)-1)/3)))
    pl.show()

    pl.contourf(xg,yg,un[len(un)-1].reshape(N,N),cmap='inferno')
    cb = pl.colorbar()
    cb.set_label(r"Degrees C$^{\circ}$")
    pl.title("Time: %s seconds"%(len(un)-1*dt))
    pl.show()

plot_four()