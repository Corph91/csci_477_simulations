"""
    Sean Corbett
    04/26/2017
    CSCI477: Simulations
    Homework4: Explicit Method
"""

import numpy as np
import scipy.sparse as sparse
import time as time
import visual as vp, vpm

N = 25  # Number of mesh points in the x and y directions.
B = 0.5
L = 1
x = np.linspace(-L/2., L/2., N)
y = np.linspace(-L/2., L/2., N)
x, y = np.meshgrid(x, y)

def gaussian(x,y,mu_x = 0,mu_y=0,sigma=.1):
    return np.exp(-(np.sqrt((x-mu_x)**2+(y-mu_y)**2))**2/(2*sigma**2))

def init():
    global u0, u1, A, boundary, Aprime
    init_value =  gaussian(x,y)
    u0, u1 = init_value.flatten(),init_value.flatten()
    A = np.diag(-4 * np.ones(N ** 2))
    A += np.diag(np.ones((N ** 2) - 1), 1) + np.diag(np.ones((N ** 2) - 1), -1)
    A += np.diag(np.ones(N * (N-1)),N)
    A += np.diag(np.ones(N * (N-1)),-N)
    I = np.diag(np.ones(N**2))

    b = range(N ** 2 - N, N ** 2)
    t = range(0, N)
    l = range(0, N ** 2 - N, N)
    r = range(N - 1, N ** 2, N)
    boundary = b + t + l + r
    A[boundary,:] = I[boundary,:]
    A = sparse.csr_matrix(A)


def update(u0, u1):
    return (B**2) * A.dot(u1) + (2 * u1) - u0


init()

scene = vp.display(title='2D waves', background=(.2,.5,1),
                   center=(L/2,L/2,0), up=(0,0,1), forward=(1,2,-1))

scene.material = vp.materials.marble # default material for all objects
net = vpm.net(x, y, 2 * u0.reshape(N,N), vp.color.yellow, 0.005)              # mesh net
#mesh = vpm.mesh(x, y, u0.reshape(N,N), (1.0,1.0,1.0), (1,0,0))   # mesh

counter = 0
start = 0
end = 0

while(True):
    if(counter == 0):
        start = time.clock()
    elif(counter == 100):
        end = time.clock()
        elapsed = end - start
        print("Time for 100 explicit iterations:",elapsed)
    vp.rate(40), vpm.wait(scene)
    un = update(u0, u1)
    un[boundary] = 0
    net.move(x, y, un.reshape(N, N))
    u0, u1 = u1,un
    counter += 1
