from numpy import *
from sys import *
import pylab as plt
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.colors import LightSource

x = genfromtxt("altitude.txt",dtype = 'f', delimiter = ' ')[: :-1]
h = 30000
dx = zeros(x.shape)
dy = zeros(x.shape)

dy[1:-1,:] = (x[2:,:] - x[:-2,:])/ (2 * h)
dx[1:-1] = (x[:-2] - x[2:])/ (2 * h)

dy[0:1,:] = (x[1:2,:] - x[0:1,:])/(2 * h)
dy[len(dx)-1:,:] = (x[len(dx)-1:,:] - x[len(dx)-2:len(dx)-1,:])/(2 * h)
dx[0:] = (x[1:2] - x[0:1])/(2 * h)
dx[len(dy)-1:] = (x[len(x)-1:] - x[len(x)-2:len(x)-1])/(2 * h)

conv = pi/180

I = ((cos(45 * conv) * dx) + (sin(45 * conv) * dy))/sqrt((dx**2)+(dy**2)+1)

ls = LightSource(azdeg=210, altdeg=5)
plt.imshow(ls.shade(I, cmap=cm.gray, vert_exag=20, blend_mode='overlay'))
plt.gca().invert_yaxis()
plt.colorbar()
plt.show()