
import pylab as pl
from scipy.integrate import quadrature
import numpy as np

h = 500.

def f(x):
    return 4 * x * (1 - x)

def phi_i(x,i,m):
    x_ = np.zeros(x.shape)
    x_[i-1] = (x[i] - x[i-1])/h
    x_[i+1] = (x[i+1] - x[i])/h
    print("A",x_)
    return x_


def phi(x, i, m):
    """
    1D FEM linear (CG) shape funtion
    INPUTS:
        x := a numpy array of coordinates to evaluate phi at
        i := the ith hat function, centered at the ith point
             in the mesh, where 0 is the first function N-1 the last
        m := a finite element mesh in the form of a numpy array
             containing the coordinates of N points on a line
    OUTPUT:
        returns phi_i(x)
    """
    return phi_i(x,i,m)


# The following function computes the sum of the basis functions times the
# values at the nodal points. Observe that the nodal point values are found
# on the first line
def interp(x,mesh,f):
    us = f(mesh)    # Values at nodal points
    ui=0
    for i in range(2,len(us)):
        ui += us[i]*phi(x,i,mesh)
    return ui

mesh = pl.linspace(0,1,6)
x = pl.linspace(0,1,500)

ui = interp(x,mesh,f)

pl.plot(x,ui,'k')
pl.plot(x,f(x),'r')
pl.plot(mesh,.5*pl.ones(mesh.size),'bo')
pl.legend(["FEM Interpolation","Analytic Values","FEM Mesh"],loc=3)
pl.title(r"FEM Interpolation of $y=4x(1-x)$")
pl.show()
